﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DKP.VR;
using Photon.Pun;
public class FollowObjectPosition : MonoBehaviour
{
    public PhotonView photonView;

    public bool isLeft = false;
    public Transform target;
    public Vector3 fixPos = Vector3.zero;

    // Update is called once per frame
    void Update()
    {
        if (photonView.IsMine && PhotonNetwork.IsConnected)
        {
            if (isLeft)
                target = VRPlatformSetting.instance.handL.transform;
            else if (!isLeft)
                target = VRPlatformSetting.instance.handR.transform;

            gameObject.transform.position = target.transform.TransformPoint(fixPos);
            gameObject.transform.rotation = target.transform.rotation;
        }
    }
}
