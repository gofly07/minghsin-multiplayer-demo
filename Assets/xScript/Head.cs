using UnityEngine;
using DKP.VR;
using Photon.Pun;
public class Head : MonoBehaviour
{
    public PhotonView photonView;
    [SerializeField] private Transform rootObject, followObject;
    [SerializeField] private Vector3 positionOffset, rotationOffset, headBodyOffset;

    private void LateUpdate()
    {
        if (photonView.IsMine && PhotonNetwork.IsConnected)
        {
            rootObject.position = transform.position + headBodyOffset;
            rootObject.forward = Vector3.ProjectOnPlane(VRPlatformSetting.instance.oVRCamera.transform.up, Vector3.up).normalized;

            transform.position = VRPlatformSetting.instance.oVRCamera.transform.TransformPoint(positionOffset);
            transform.rotation = VRPlatformSetting.instance.oVRCamera.transform.rotation * Quaternion.Euler(rotationOffset);
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
