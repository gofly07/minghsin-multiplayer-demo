using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITest : MonoBehaviour
{

    [SerializeField] private Button bt;

    // Start is called before the first frame update
    void Start()
    {
        bt.onClick.AddListener(delegate { Click(); });
    }

    private void Click()
    {
        Debug.Log(bt.gameObject.name + " is clicked");
    }

    public void OnClick2()
    {
        Debug.Log("bt is clicked!!");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
