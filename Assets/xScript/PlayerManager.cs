﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DKP.VR;
using Photon.Pun;
using ExitGames.Client.Photon;
using Photon.Realtime;

public class PlayerManager :  MonoBehaviour
{
    private PhotonView photonView;
    public Transform head;
    public Transform handL;
    public Transform handR;

    private void Start()
    {
        photonView = gameObject.GetComponent<PhotonView>();
    }

    // Update is called once per frame
    private void FixedUpdate()
    {

        if (photonView.IsMine && PhotonNetwork.IsConnected)
        {
           // BodyFollowing();

            if (OVRInput.GetDown(OVRInput.Button.PrimaryIndexTrigger, OVRInput.Controller.RTouch))
                photonView.RPC("ChangeFloorColor", RpcTarget.All);


        }
    }

    private void BodyFollowing()
    {
        head.transform.position = VRPlatformSetting.instance.oVRCamera.transform.position;
        head.transform.rotation = VRPlatformSetting.instance.oVRCamera.transform.rotation;

        handL.transform.position = VRPlatformSetting.instance.handL.transform.position;
        handL.transform.rotation = VRPlatformSetting.instance.handL.transform.rotation;

        handR.transform.position = VRPlatformSetting.instance.handR.transform.position;
        handR.transform.rotation = VRPlatformSetting.instance.handR.transform.rotation;
    }

    [PunRPC]
    private void ChangeFloorColor()
    {
        GameObject.Find("Plane").GetComponent<Renderer>().material.color = Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }
}
