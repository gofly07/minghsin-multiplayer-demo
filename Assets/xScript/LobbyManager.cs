﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Realtime;
using Photon.Pun;
using ExitGames.Client.Photon;

public class LobbyManager : MonoBehaviourPunCallbacks
{
    public string roomName = "Room1";

    void Awake()
    {
        // #Critical
        // this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.AutomaticallySyncScene = true;
    }

    private void Start()
    {
        Connect();
    }

    public void Connect()
    {
        //if (PhotonNetwork.IsConnected)
        //{
        //    PhotonNetwork.JoinRandomRoom();
        //}
        //else
        //{
            PhotonNetwork.GameVersion = "1";
            PhotonNetwork.PhotonServerSettings.DevRegion = "asia";
            PhotonNetwork.PhotonServerSettings.PunLogging = PunLogLevel.ErrorsOnly;
            PhotonNetwork.PhotonServerSettings.RunInBackground = true;
            PhotonNetwork.PhotonServerSettings.EnableSupportLogger = false;

            PhotonNetwork.PhotonServerSettings.AppSettings.FixedRegion = "asia";
            PhotonNetwork.PhotonServerSettings.AppSettings.UseNameServer = true;
            PhotonNetwork.PhotonServerSettings.AppSettings.AppIdRealtime = "105e3fa7-ff5b-4339-987c-8ada0fba218f";
            //PhotonNetwork.PhotonServerSettings.AppSettings.AppIdVoice = "6969b726-22fe-448f-988a-da68c6efa324";
            PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion = "1";
            PhotonNetwork.PhotonServerSettings.AppSettings.Protocol = ConnectionProtocol.Udp;
            PhotonNetwork.PhotonServerSettings.AppSettings.NetworkLogging = DebugLevel.ERROR;

            PhotonNetwork.ConnectUsingSettings();
        //}
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster() was called by PUN. This client is now connected to Master Server in region [" + PhotonNetwork.CloudRegion + "] and can join a room. Calling: PhotonNetwork.JoinRandomRoom();");
        //PhotonNetwork.JoinRandomRoom();
        //PhotonNetwork.JoinRoom("b8d4ac61-451b-4582-96d4-39241e0d6220");

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 5;
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;


        PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions,TypedLobby.Default);

    }

    public override void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby(). This client is now connected to Relay in region [" + PhotonNetwork.CloudRegion + "]. This script now calls: PhotonNetwork.JoinRandomRoom();");
        //PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRandomFailed() was called by PUN. No random room available in region [" + PhotonNetwork.CloudRegion + "], so we create one. Calling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");

        //RoomOptions roomOptions = new RoomOptions() { MaxPlayers = 5 };
        //PhotonNetwork.CreateRoom(null, roomOptions, null);

    }

    public override void OnJoinedRoom()
    {
        Debug.Log("PUN Basics Tutorial/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.\nFrom here on, your game would be running.");

        // #Critical: We only load if we are the first player, else we rely on  PhotonNetwork.AutomaticallySyncScene to sync our instance scene.
        //  if (PhotonNetwork.CurrentRoom.PlayerCount == 1)
        //  {
        Debug.Log("We load the 'Room for 1' ");

        // #Critical
        // Load the Room Level. 
        PhotonNetwork.LoadLevel("Scene_main");


        Debug.Log(PhotonNetwork.NetworkingClient.LoadBalancingPeer.DisconnectTimeout);
        PhotonNetwork.NetworkingClient.LoadBalancingPeer.DisconnectTimeout = 30000;
        PhotonNetwork.NetworkingClient.LoadBalancingPeer.CrcEnabled = true;
        PhotonNetwork.NetworkingClient.LoadBalancingPeer.MaximumTransferUnit = 520;
        // }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        Debug.Log(newPlayer.UserId);
    }
}
